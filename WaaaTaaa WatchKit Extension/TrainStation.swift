//
//  TrainStation.swift
//  WaaaTaaa WatchKit Extension
//
//  Created by Apple Student on 21/01/2020.
//  Copyright © 2020 Michele Navolio. All rights reserved.
//

import SwiftUI

struct TrainStation {
    let id: String
    let name: String
    let location: CLLocationCoordinate2D
}

//struct TrainStation_Previews: PreviewProvider {
//    static var previews: some View {
//        TrainStation()
//    }
//}
