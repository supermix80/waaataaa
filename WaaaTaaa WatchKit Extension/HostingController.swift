//
//  HostingController.swift
//  WaaaTaaa WatchKit Extension
//
//  Created by Michele Navolio on 17/01/2020.
//  Copyright © 2020 Michele Navolio. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
