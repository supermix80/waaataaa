//
//  NotificationView.swift
//  WaaaTaaa WatchKit Extension
//
//  Created by Michele Navolio on 17/01/2020.
//  Copyright © 2020 Michele Navolio. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
