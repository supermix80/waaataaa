//
//  ContentView.swift
//  WaaaTaaa WatchKit Extension
//
//  Created by Michele Navolio on 17/01/2020.
//  Copyright © 2020 Michele Navolio. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            NavigationLink(destination: NavigationView()) {
                HStack {
                    Text("Navigate to:")
                    Spacer()
                    Image(systemName: "location.fill")
                    
                }
            }
            .background(/*@START_MENU_TOKEN@*/Color.blue/*@END_MENU_TOKEN@*/)
            .cornerRadius(15)
            //            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
            //                HStack {
            //                    Text("Navigate to")
            //                    Spacer()
            //                    Image(systemName: "location.fill")
            //
            //                }
            //            }
            //            .background(/*@START_MENU_TOKEN@*/Color.blue/*@END_MENU_TOKEN@*/)
            //            .cornerRadius(5)
            
            NavigationLink(destination: Test()) {
                Text("Map")
            }
            List(/*@START_MENU_TOKEN@*/0 ..< 5/*@END_MENU_TOKEN@*/) { item in
                Text("Hello, World!")
            }
        }
        .navigationBarTitle(Text("Pippo"))
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct Test: View {
    @State var scrollAmount = 0.0
    
    var body: some View {
        WatchMapView()
        .focusable()
        .digitalCrownRotation($scrollAmount)
        .navigationBarTitle(Text("This is a map!"))
    }
}

struct NavigationView: View{
    var body: some View{
        VStack{
            Text("NavigationView")
        }
    .navigationBarTitle(Text("Altro"))
    }
}

struct NavigationView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView()
    }
}
