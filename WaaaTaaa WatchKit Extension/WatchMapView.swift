//
//  WatchMapView.swift
//  WaaaTaaa WatchKit Extension
//
//  Created by Ammad on 20/01/2020.
//  Copyright © 2020 Michele Navolio. All rights reserved.
//

import SwiftUI
import CoreLocation

struct WatchMapView: WKInterfaceObjectRepresentable {
    func makeWKInterfaceObject(context: WKInterfaceObjectRepresentableContext<WatchMapView>) -> WKInterfaceMap {
        return WKInterfaceMap()
    }
    
    func updateWKInterfaceObject(_ map: WKInterfaceMap, context: WKInterfaceObjectRepresentableContext<WatchMapView>) {
        let locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter  = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        
        let span = MKCoordinateSpan(latitudeDelta: 0.02,
            longitudeDelta: 0.02)
        
        let station: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 40.8313, longitude: 14.3079)
        
        let region = MKCoordinateRegion(
            center: locationManager.location!.coordinate,
            span: span)
        
        map.setRegion(region)
        map.setShowsUserLocation(true)
        
    }
}

struct WatchMapView_Previews: PreviewProvider {
    static var previews: some View {
        WatchMapView()
    }
}
